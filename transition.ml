open Containers

module State = struct
  type t = MkState : {state:'a; update: 'a -> int -> 'a option} -> t

  let update (MkState {state;update}) time =
    match update state time with
    | Some state -> Some (MkState {state; update})
    | None -> None
end

let identity = State.MkState {state=(); update=fun () _ -> None}

module Delay: sig val of_ : delay:int -> State.t end = struct
  let of_ ~delay =
    let update time delta = let time = time + delta in if time < delay then Some time else None in
    State.MkState {state=0; update}
end

module Interpolate (S: sig
    type t
    val (-) : t -> t -> t
    val (+.) : t -> float -> t
    val ( * ) : t -> float -> float
  end ) = struct
  open S

  let between s ~start ~stop ~delay =
    let distance = stop - start in
    let update time delta =
      let time = time + delta in
      let proportion = Float.(of_int time / of_int delay) in
      if Float.(proportion > 1.0)
      then (s stop; None)
      else (s (start +. distance * proportion); Some time) in
    State.MkState {state=0; update}

end

module InterpolateInt = Interpolate (struct
    type t = int
    let (-) = Int.(-)
    let (+.) a b = Float.(to_int @@ of_int a + b)
    let ( * ) a b = Float.(of_int a * b)
  end)

module InterpolateFloat = Interpolate (struct
    type t = float
    let (-) = Float.(-)
    let (+.) a b = Float.(a + b)
    let ( * ) a b = Float.(a * b)
  end)

module Combine = struct
  let in_parallel (State.MkState { state=s_a; update=u_a }: State.t) (State.MkState { state=s_b; update=u_b }: State.t) =
    let update (o_sa, o_sb) time =
      let o_sa = Option.bind o_sa (Fun.flip u_a time) in
      let o_sb = Option.bind o_sb (Fun.flip u_b time) in
      match o_sa, o_sb with
      | None, None -> None
      | _ -> Some (o_sa, o_sb) in
    State.MkState {state=(Some s_a,Some s_b); update}

  let in_sequence (State.MkState { state=s_a; update=u_a }: State.t) (State.MkState { state=s_b; update=u_b }: State.t) =
    let update (o_sa, o_sb) time =
      match o_sa, o_sb with
      | None, None -> None
      | Some sa, _ -> Some (u_a sa time, o_sb)
      | None, Some sb -> Some (None, u_b sb time) in
    State.MkState {state=(Some s_a,Some s_b); update}

end

include State
