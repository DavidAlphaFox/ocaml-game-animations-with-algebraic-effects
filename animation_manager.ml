open Containers

module Hashtbl = Hashtbl.Make (Id)

type t = Animation.t Hashtbl.t

let manager : t = Hashtbl.create 1000

module type ID = sig type t val id : t -> Id.t end

let add_animation (type a) (module S: ID with type t = a) (vl: a) anim =
  Hashtbl.add manager (S.id vl) anim

let clear_animations (type a) (module S: ID with type t = a) (vl: a) =
  let anims = Hashtbl.find_all manager (S.id vl)  in
  anims |> List.iter (fun _ -> Hashtbl.remove manager (S.id vl));
  anims

let update_animations time =
  Hashtbl.filter_map_inplace (fun _ anim -> Animation.update time anim) manager
