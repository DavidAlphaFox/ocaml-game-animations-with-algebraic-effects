[@@@warning "-33-27"]
open Containers

let circle_animation () =
  let module State = Circle_manager.Make (Animated_circle) in
  let module System = System.Make (State) in
  System.main ()

let menu_animation () =
  let module State = Menu_manager.Make (Animated_menu) in
  let module System = System.Make (State) in
  System.main ()

let grid_animation () =
  let module State = Grid_manager.Make (Animated_graph) in
  let module System = System.Make (State) in
  System.main ()

let () =
  (* circle_animation (); *)
  (* menu_animation (); *)
  (* grid_animation (); *)
  ()
