type coords = Coordinate_system.coords

module Make
    (View: sig
       type t
       val update: Coordinate_system.t -> t -> Graphics.status -> int -> bool
       val draw: Coordinate_system.t -> t -> unit
       val init: t
     end)  = struct

  type state = UnClicked | Clicked of int * int

  type t = View.t * Coordinate_system.t * state
  let _dim = 100

  let update ((view, grid, state):t) (s: Graphics.status) time : t =
    match state with
    | Clicked (old_x, old_y) ->
      if Graphics.button_down () then begin
        let dx = s.mouse_x - old_x in
        let dy = s.mouse_y - old_y in
        (view, {x=grid.x - dx; y=grid.y - dy}, Clicked (s.mouse_x, s.mouse_y))
      end else (view, grid, UnClicked)
    | UnClicked ->
      match View.update grid view s time with
      | true -> (view, grid, state)
      | false ->
        let mouse_pressed = Graphics.button_down () in
        if mouse_pressed
        then (view, grid, Clicked (s.mouse_x, s.mouse_y))
        else (view, grid, UnClicked)

  let draw (view, grid, _) = View.draw grid view

  let init:t = (View.init, {x=0;y=0}, UnClicked)

end
