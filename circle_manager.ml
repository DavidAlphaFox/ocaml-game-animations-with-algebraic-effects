module Make (Circle: sig type t val update: t -> Graphics.status -> int -> unit val draw: t -> unit val create: int -> int -> t end)  = struct
  type t = Circle.t list
  let _dim = 100

  let update circs (Graphics.{mouse_x; mouse_y; _} as s) time =
    List.iter (fun t -> Circle.update t s time) circs;
    if Graphics.key_pressed () then begin
      ignore @@ Graphics.read_key ();
      (Circle.create mouse_x mouse_y :: circs)
    end else circs
    
  let draw circs = List.iter (Circle.draw) circs

  let init = []

end
