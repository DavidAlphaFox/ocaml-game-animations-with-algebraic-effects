open Containers
module type STATE = sig
    type t
    val init : t
    val update: t -> Graphics.status -> int -> t
    val draw: t -> unit
  end

module Make (S: STATE) = struct

  let frames_per_second = 64

  let ms_per_second = 1000

  let ms_per_frame = ms_per_second / frames_per_second



  let main () =
    let time () = Mtime_clock.now () in
    let rec loop (last_time, last_drawn, should_exit, state) =
      let current_time = time () in
      let delta = Int.of_float (Mtime.Span.to_ms @@ Mtime.span last_time current_time) in
      if not should_exit
      then begin
        let should_exit, state = match Graphics.wait_next_event Graphics.[Key_pressed; Poll] with
          | { key = 'q'; _ } -> (true, state)
          | s -> (should_exit, S.update state s delta) in
        Animation_manager.update_animations delta;
        if Int.of_float (Mtime.Span.to_ms @@ Mtime.span last_drawn current_time) > ms_per_frame then begin
          Graphics.set_color Graphics.black;
          Graphics.fill_rect 0 0 (Graphics.size_x ()) (Graphics.size_y ());
          S.draw state;
          loop (current_time, current_time, should_exit, state)
        end else
          loop (current_time, last_drawn, should_exit, state)
      end
      else Graphics.close_graph () in
    Graphics.open_graph "";
    Graphics.auto_synchronize true;
    loop (time (), time (), false, S.init)
end
