effect Animation : Transition.t * (unit -> unit) option -> unit

type s = {current_state: Transition.t; kont: (unit, s) continuation option; on_cancellation: (unit -> unit) option }

type t = s

let run ?on_cancellation (x: Transition.t) : unit = perform (Animation (x, on_cancellation))
let return (s: Transition.t) : s = {current_state=s; kont=None; on_cancellation=None}


let build (f: unit -> s) : t =
  try f ()  with
  | effect (Animation (state, on_cancellation)) kont ->
    {current_state=state; kont=Some kont; on_cancellation}

let update time (t: t) =
  match Transition.update t.current_state time with
  | Some state -> Some {t with current_state = state}
  | None -> match t.kont with
    | None -> None
    | Some kont ->
      Some (build (fun () -> continue kont ()))
