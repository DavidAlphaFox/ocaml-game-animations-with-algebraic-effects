type coords = int * int

type t = {x: int; y: int; }

let from_world grid (x,y) =
  Graphics.size_x ()/2 - grid.x + x, Graphics.size_y ()/2 - grid.y + y

let from_display grid (x,y) = x - Graphics.size_x ()/2 + grid.x, y - Graphics.size_y ()/2 + grid.y
