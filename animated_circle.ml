let circle_bounce_anim  (circle: Circle.t) = 
  let rec loop () = 
  let open Animation in
  (* move circle down *)
  circle.state <- Moving;
  run @@ Transition.(InterpolateInt.between ~start:circle.y ~stop:(circle.y - 200) ~delay:300 (fun y -> circle.y <- y));
  circle.state <- Squashed;

  run @@ 
    Transition.(Combine.in_parallel
      (InterpolateInt.between ~start:circle.w ~stop:(circle.w + 30) ~delay:100 (fun y -> circle.w <- y))
      (InterpolateInt.between ~start:circle.h ~stop:(circle.h - 30) ~delay:50 (fun y -> circle.h <- y)));
  run @@
    Transition.(Combine.in_parallel
      (InterpolateInt.between ~start:circle.w ~stop:(circle.w - 30) ~delay:120 (fun y -> circle.w <- y))
      (InterpolateInt.between ~start:circle.h ~stop:(circle.h + 30) ~delay:80 (fun y -> circle.h <- y)));
  circle.state <- Moving;
  run @@ Transition.(InterpolateInt.between ~start:circle.y ~stop:(circle.y + 200) ~delay:300 (fun y -> circle.y <- y));
  circle.state <- Stationary;
  run @@ Transition.(Delay.of_ ~delay:50);
  loop () in
  Animation.build (loop)

type state = None | Hover of Animation.t | Clicked

type t = {
  circle: Circle.t;
  mutable state: state
}

let circle_selected = ref false

let update _circ (_s: Graphics.status) _time =
  let mouse_within = Circle.within _circ.circle _s.mouse_x _s.mouse_y in
  let mouse_down = Graphics.button_down () in
  match _circ.state with
  | None when mouse_within ->
    let anim = List.nth (Animation_manager.clear_animations (module Circle) _circ.circle) 0 in
    _circ.state <- Hover anim
  | Hover _ when mouse_down && not !circle_selected ->
    _circ.state <- Clicked;
    circle_selected := true;
  | Hover anim when not mouse_within ->
    Animation_manager.add_animation (module Circle) _circ.circle anim;
    _circ.state <- None
  | Clicked when not mouse_down ->
    Animation_manager.add_animation (module Circle) _circ.circle (circle_bounce_anim _circ.circle);
    circle_selected := false;
    _circ.state <- None;
  | Clicked ->
    _circ.circle.x <- _s.mouse_x;
    _circ.circle.y <- _s.mouse_y;
  | _ -> ()

let draw {circle=Circle.{x;y;w;h;state;_};_} =
  begin match state with
    | Circle.Stationary -> Graphics.set_color (Graphics.rgb 100 0 0)
    | Circle.Moving -> Graphics.set_color (Graphics.rgb 125 0 0)
    | Circle.Squashed -> Graphics.set_color (Graphics.rgb 150 0 0)
  end;
  Graphics.fill_ellipse x y (abs w) (abs h)

let create x y =
  let circle = Circle.create ~x ~y ~w:100 ~h:100 in
  Animation_manager.add_animation (module Circle) circle (circle_bounce_anim circle);
  {circle;state=None} 

