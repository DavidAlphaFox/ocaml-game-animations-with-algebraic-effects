open Containers


  type state = Stationary | Moving | Squashed

type t = {
  id: Id.t;
  mutable state: state;
  mutable x: int; mutable y: int;
  mutable w: int; mutable h: int;
}

  let create ~x ~y ~w ~h = {id=Id.next_id (); x;y;w;h; state=Stationary}

  let id s = s.id

  let within {x=h;y=k;w=a;h=b;_} x y =
    let (^) a b = Stdlib.Float.pow a b in
    let h = Float.of_int h in
    let k = Float.of_int k in
    let a = Float.of_int a in
    let b = Float.of_int b in
    let x = Float.of_int x in
    let y = Float.of_int y in
    Float.((( x -  h) ^ 2.0) / (a ^ 2.0) + ((y - k)^2.0)/ (b ^ 2.0) <= 1.0 )


